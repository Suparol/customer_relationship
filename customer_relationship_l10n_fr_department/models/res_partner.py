from odoo import models


class ResPartner(models.Model):
	_inherit = "res.partner"

	# Address copying

	def _address_fields(self):
		return super()._address_fields() + ['department_id']

	def _copy_address_from(self, source):
		# Adding field department_id
		for record in self:
			record.department_id = source.department_id
		return super()._copy_address_from(source)
