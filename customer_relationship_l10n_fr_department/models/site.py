# -*- coding: utf-8 -*-

from odoo import models, fields, api

class CustomerSite(models.Model):
	_inherit = "customer.site"

	# Address
	department_id = fields.Many2one("res.country.department", string="Département")

	def _address_fields(self):
		return super()._address_fields() + ['department_id']

	# Address
	# There is a inclusion relation that allows to deduce other fields from the completion of some of them:
	#    zip < department < state < country
	# TODO: Check if onchanges are called recursively. If so, check: - When?; - Can this generate loops of some sort?
	# TODO: This does not allow exceptions. See e.g. https://fr.wikipedia.org/wiki/Code_postal_en_France#Cas_particuliers
	# TODO: This only works for France (and it assumes that a country called "France" is France)
	# TODO: It should probably be an option to disable auto-completion by zipcode for France...
	# TODO: Add cities to these?
	# TODO: Not very smart when changing the country...

	@api.onchange('zip')
	def _onchange_zip(record):
		if record.country_id.name == "France" and record.zip and len(record.zip) == 5:
			department_code = record.zip[:2]
			if not record.department_id or record.department_id.code != department_code:
				departments = record.env['res.country.department'].search([
						('code', '=', department_code),
						('country_id', '=', record.country_id.id)],
					limit=2)
				if len(departments) == 1:
					record.department_id = departments[0]
				elif record.department_id:
					record.department_id = False

	@api.onchange('department_id')
	def _onchange_department_id(record):
		if record.department_id and record.department_id.state_id != record.state_id:
			record.state_id = record.department_id.state_id
		if record.country_id.name == "France" and record.zip and len(record.zip) == 5 and (not record.department_id or record.zip[:2] != record.department_id.code):
			record.zip = False

	@api.onchange('state_id')
	def _onchange_state_id(record):
		super()._onchange_state_id()
		if record.department_id and record.department_id.state_id != record.state_id:
			record.department_id = False
