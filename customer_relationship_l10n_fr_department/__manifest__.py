# -*- coding: utf-8 -*-

{
	'name': 'Relations clients - Départements français',
	'application': False,
	'depends': [
		'customer_relationship',
		'l10n_fr_department',
	],
	'auto_install': True,
	'license': 'GPL-3',
	'data': [
		'views/company_views.xml',
		'views/contact_views.xml',
		'views/site_views.xml',
	],
	'assets': {
		'web.assets_backend': [
			'customer_relationship_l10n_fr_department/static/scss/address.scss',
		]
	}
}
