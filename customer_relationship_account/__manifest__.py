# -*- coding: utf-8 -*-

{
	'name': 'Relations clients - Facturation',
	'application': False,
	'depends': [
		'customer_relationship',
		'account',
	],
	'auto_install': True,
	'license': 'GPL-3',
	'data': [
		'views/site_views.xml',
	],
}
