# -*- coding: utf-8 -*-

from odoo import models, fields
from odoo import Command
from odoo import exceptions

from datetime import timedelta

class MaintenanceContract(models.Model):
	_inherit = "customer.maintenance.contract"

	# ======
	# Fields
	# ======

	# Invoice
	invoice_id = fields.Many2one("account.move", string="Facture")

	# =======
	# Actions
	# =======

	def action_generate_invoice(self):
		for record in self:
			if record.invoice_id:
				raise exceptions.UserError("Une facture existe déjà pour ce contrat")

		invoices = self._generate_invoice()
		# TODO: This works only if the records are returned in the same order they were
		# asked to be created.
		for record, invoice in zip(self, invoices):
			record.invoice_id = invoice

		return True

	def _generate_invoice(self):
		invoice_vals_list = []
		for record in self:
			invoice_vals = {}

			# TODO: Check if this field is well defined?
			invoice_vals['partner_id'] = record.site_id.company_id

			invoice_vals['move_type'] = 'out_invoice'

			invoice_lines_vals = []

			contract_duration = (record.end + timedelta(days=1)) - record.start

			if record.hotline:
				invoice_lines_vals.append(Command.create({
						'name':'Hotline',
						'price_unit': 1. * contract_duration.days,
					}))
			if record.maintenance:
				invoice_lines_vals.append(Command.create({
						'name':'Maintenance',
						'price_unit': 0.5 * contract_duration.days,
					}))
			if record.report:
				invoice_lines_vals.append(Command.create({
						'name':'Rapport de maintenance',
						'price_unit': 0.5 * contract_duration.days,
					}))
			invoice_vals['invoice_line_ids'] = invoice_lines_vals

			invoice_vals_list.append(invoice_vals)

		return self.env['account.move'].create(invoice_vals_list)
