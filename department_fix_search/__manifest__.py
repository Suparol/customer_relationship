# -*- coding: utf-8 -*-

{
	'name': 'Department fix for restricted search',
	'depends': [
		'base',
		'l10n_fr_department',
	],
	'license': 'GPL-3',
}
