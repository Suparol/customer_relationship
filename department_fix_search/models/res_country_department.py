# -*- coding: utf-8 -*-

from odoo import models, api
from odoo.osv import expression

class ResCountryDepartment(models.Model):
	_inherit = "res.country.department"

	# Inspired from res.country.state
	@api.model
	def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
		# In addition to usual search, `name` can be department 'code' (using =ilike operator).
		# Context variables 'country_id' and 'state_id' can be specified,
		#   restricting search to the corresponding country or state respectively.

		args = args or []
		if self.env.context.get('country_id'):
			args = expression.AND([args, [('country_id', '=', self.env.context.get('country_id'))]])
		if self.env.context.get('state_id'):
			args = expression.AND([args, [('state_id', '=', self.env.context.get('state_id'))]])

		if operator == 'ilike' and not (name or '').strip():
			first_domain = []
			domain = []
		else:
			first_domain = [('code', '=ilike', name)]
			domain = [('name', operator, name)]

		first_department_ids = self._search(expression.AND([first_domain, args]), limit=limit, access_rights_uid=name_get_uid) if first_domain else []
		return list(first_department_ids) + [
				department_id
				for department_id in self._search(expression.AND([domain, args]), limit=limit, access_rights_uid=name_get_uid)
				if department_id not in first_department_ids
			]
