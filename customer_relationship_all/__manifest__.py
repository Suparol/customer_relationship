# -*- coding: utf-8 -*-

{
	'name': 'Relations clients - Extras',
	'application': False,
	'depends': [
		'customer_relationship',
		'customer_relationship_l10n_fr_department',
		'customer_relationship_partner_firstname',
		'customer_relationship_account',
		'department_fix_search',
	],
	'license': 'GPL-3',
}
