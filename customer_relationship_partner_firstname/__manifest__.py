# -*- coding: utf-8 -*-

{
	'name': 'Relations clients - Nom/Prénom',
	'application': False,
	'depends': [
		'customer_relationship',
		'partner_firstname',
	],
	'auto_install': True,
	'license': 'GPL-3',
	'data': [
		'views/contact_views.xml',
	],
}
