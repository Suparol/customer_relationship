from odoo import api, fields, models

class ResPartner(models.Model):
	_inherit = "res.partner"

	# Only one of them is used, depending of if the partner is a company or an individual
	company_site_ids = fields.One2many("customer.site", "company_id", string="Sites du compte")
	contact_site_ids = fields.One2many("customer.site", "contact_id", string="Sites du contact")


	# Address copying

	def _copy_address_from(self, source):
		"""
		Copy address from the source.
		Both `source` is an individual records (i.e. a "singleton" recordset), while `self` may be a recordset.
		"""
		address_fields = self._address_fields()
		for record in self:
			for field in address_fields:
				record[field] = source[field]

	def action_copy_address_from_parent(self):
		for record in self:
			record._copy_address_from(record.parent_id)
		return True
