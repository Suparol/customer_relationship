# -*- coding: utf-8 -*-

from . import res_partner
from . import site
from . import maintenance_contract
from . import market_segment
