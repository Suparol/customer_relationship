# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo import exceptions

from dateutil import relativedelta as rd

class MaintenanceContract(models.Model):
	_name = "customer.maintenance.contract"
	_description = "Contrat de maintenance"

	# ======
	# Fields
	# ======

	site_id = fields.Many2one("customer.site", string="Site associé", required=True)

	# Contract guarantees
	hotline = fields.Boolean(string="Hotline", default=False)
	maintenance = fields.Boolean(string="Visite de maintenance", default=False)
	report = fields.Boolean(string="Rapport", default=False)

	# Dates (start and end both included)
	start = fields.Date(string="Début du contrat", required=True)
	end = fields.Date(string="Fin du contrat", required=True, compute="_compute_end")
	duration_unit = fields.Selection([
			('days', 'Jours'),
			('months', 'Mois'),
			('trimesters', 'Trimestres'),
			('years', 'Années'),
		], default='years')
	duration = fields.Integer(string="Durée du contrat")


	# Computations

	@api.depends('start', 'duration', 'duration_unit')
	def _compute_end(self):
		for record in self:
			if not record.start:
				record.end = record.start
				continue

			unit = record.duration_unit
			duration = record.duration
			if not unit or not duration:
				delta = rd.relativedelta(days=1)
			elif unit == 'days':
				delta = rd.relativedelta(days=duration)
			elif unit == 'months':
				delta = rd.relativedelta(months=duration)
			elif unit == 'trimesters':
				delta = rd.relativedelta(months=3*duration)
			elif unit == 'years':
				delta = rd.relativedelta(years=duration)
			else:
				raise exceptions.UserError("Unité " + unit + " non supportée")

			record.end = (record.start + delta) - rd.relativedelta(days=1)
