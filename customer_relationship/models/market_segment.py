# -*- coding: utf-8 -*-

from odoo import models, fields

class MarketSegment(models.Model):
	_name = "customer.market_segment"
	_description = "Segment de marché"
	_order = "sequence"

	# ======
	# Fields
	# ======

	name = fields.Char(string="Nom", required=True)
	sequence = fields.Integer("Sequence", default=1)

	_sql_constraints = [
		('name_uniq', 'unique (name)',
			'Le nom d’un segment de marché doit être unique.'),
	]
