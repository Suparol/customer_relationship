# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError

from odoo.addons.base.models import res_partner

class CustomerSite(models.Model):
	_name = "customer.site"
	_description = "Site client"

	# ======
	# Fields
	# ======

	name = fields.Char(string="Nom", required=True)
	active = fields.Boolean(required=True, default=True)

	# Contacts
	company_id = fields.Many2one("res.partner", string="Propriétaire",
		domain="[('is_company', '=', True)]")
	contact_id = fields.Many2one("res.partner", string="Contact",
		domain="[('is_company', '=', False), \
			 ('parent_id', '=?', company_id)]")
	maintenance_company_id = fields.Many2one("res.partner", string="Entreprise de maintenance",
		domain="[('is_company', '=', True)]")
	maintenance_contact_id = fields.Many2one("res.partner", string="Contact de maintenance",
		domain="[('is_company', '=', False), \
			 ('parent_id', '=?', maintenance_company_id)]")

	# Address
	street = fields.Char(string="Rue")
	street2 = fields.Char(string="Rue 2")
	city = fields.Char(string="Ville")
	zip = fields.Char(string="Code postal")
	country_id = fields.Many2one("res.country", string="Pays", default=lambda self: self._default_country())
	state_id = fields.Many2one("res.country.state", string="Région")

	warning_messages = fields.Text(compute="_compute_warning_messages")
	warning_message_short = fields.Text(compute="_compute_warning_messages")
	warning_count = fields.Integer(compute="_compute_warning_messages")

	# Contract
	contract_ids = fields.One2many("customer.maintenance.contract", "site_id", string="Contrats")
	# The following is used for contract renewal
	_last_contract_id = fields.Many2one("customer.maintenance.contract", compute="_compute_last_contract")
	last_contract_hotline = fields.Boolean(related="_last_contract_id.hotline")
	last_contract_maintenance = fields.Boolean(related="_last_contract_id.maintenance")
	last_contract_report = fields.Boolean(related="_last_contract_id.report")

	# Others
	market_segment_id = fields.Many2one("customer.market_segment", string="Segment de marché")

	# ========================
	# Computations and actions
	# ========================

	def _address_fields(self):
		return list(res_partner.ADDRESS_FIELDS)

	# Dirty hack to display button in tree view without raising error on click
	# TODO: Clean this hack (ideally, find a way to display the icon properly)
	def action_noop(self):
		return True

	# Partners and contacts

	@api.onchange('company_id')
	def _onchange_company_id(site):
		if site.contact_id and site.contact_id.parent_id != site.company_id:
			site.contact_id = False

	@api.onchange('contact_id')
	def _onchange_contact_id(site):
		if site.contact_id and site.contact_id.parent_id and site.contact_id.parent_id != site.company_id:
			site.company_id = site.contact_id.parent_id

	@api.onchange('maintenance_company_id')
	def _onchange_maintenance_company_id(site):
		if site.maintenance_contact_id and site.maintenance_contact_id.parent_id != site.maintenance_company_id:
			site.maintenance_contact_id = False

	@api.onchange('maintenance_contact_id')
	def _onchange_maintenance_contact_id(site):
		if site.maintenance_contact_id and site.maintenance_contact_id.parent_id and site.maintenance_contact_id.parent_id != site.maintenance_company_id:
			site.maintenance_company_id = site.maintenance_contact_id.parent_id

	# Address

	@api.onchange('state_id')
	def _onchange_state_id(record):
		if record.state_id and record.state_id.country_id != record.country_id:
			record.country_id = record.state_id.country_id

	@api.onchange('country_id')
	def _onchange_country_id(record):
		if record.state_id and record.state_id.country_id != record.country_id:
			record.state_id = False

	# TODO: Add an option to manage default country?
	def _default_country(self):
		france_ids = self.env['res.country'].name_search('France', operator='=', limit=2)
		print(france_ids)
		if len(france_ids) != 1:
			# Default exit if France does not exists or there are too many Frances.. (should not happen?)
			return None
		else:
			print(france_ids[0])
			print(france_ids[0][0])
			return france_ids[0][0]

	# Address copying

	def _copy_address_from(self, source):
		"""
		Copy address from the source.
		Both `source` is an individual records (i.e. a "singleton" recordset), while `self` may be a recordset.
		"""
		address_fields = self._address_fields()
		for record in self:
			for field in address_fields:
				record[field] = source[field]

	def action_copy_address_from_company(self):
		for record in self:
			record._copy_address_from(record.company_id)
		return True

	def action_copy_address_from_contact(self):
		for record in self:
			record._copy_address_from(record.contact_id)
		return True


	# Contracts

	@api.depends("contract_ids")
	def _compute_last_contract(self):
		for record in self:
			contracts = record.contract_ids
			if not contracts:
				last = False
			else:
				last = contracts[0]
				for contract in contracts:
					if last.end < contract.end:
						last = contract
			record._last_contract_id = last


	# Warnings
	# TODO: Add warnings for address

	@api.depends(
		"company_id", "company_id.active", "company_id.is_company",
		"contact_id", "contact_id.active", "contact_id.parent_id", "contact_id.is_company",
		"maintenance_company_id", "maintenance_company_id.is_company",
		"maintenance_contact_id", "maintenance_contact_id.active", "maintenance_contact_id.parent_id", "maintenance_company_id.is_company")
	def _compute_warning_messages(self):
		for site in self:
			# TODO: check if it is ok in case of delete of a field
			# TODO: check if some logic is redundant
			warnings = []

			if not site.company_id:
				warnings.append("Un site doit avoir un propriétaire")
			else:
				if not site.company_id.active:
					warnings.append("Le propriétaire du site est inactif")
				if not site.company_id.is_company:
					warnings.append("Le propriétaire du site doit être une société")
			if not site.contact_id:
				warnings.append("Un site doit avoir un contact")
			else:
				if not site.contact_id.active:
					warnings.append("Le contact du site est inactif")
				if site.contact_id.is_company:
					warnings.append("Le contact du site doit être un particulier")
				if site.contact_id.parent_id != site.company_id:
					warnings.append("Le contact du site ne correspond pas au propriétaire")

			if site.maintenance_company_id:
				if not site.maintenance_company_id.is_company:
					warnings.append("La société de maintenance doit être une société")

			if site.maintenance_contact_id:
				if not site.maintenance_contact_id.active:
					warnings.append("Le contact de maintenance est incatif")
				if site.maintenance_contact_id.is_company:
					warnings.append("Le contact de maintenance doit être un particulier")
				if site.maintenance_contact_id.parent_id != site.maintenance_company_id:
					warnings.append("Le contact de maintenance ne correspond pas à la société de maintenance")

			site.warning_count = len(warnings)
			if warnings:
				site.warning_messages = "\n".join(warnings)
				site.warning_message_short = warnings[0]
			else:
				site.warning_messages = False
				site.warning_message_short = False
