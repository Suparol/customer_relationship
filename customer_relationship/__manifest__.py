# -*- coding: utf-8 -*-

{
	'name': 'Relations clients',
	'category': 'Sales',
	'application': True,
	'depends': [
		'base',
	],
	'license': 'GPL-3',
	'data': [
		'security/ir.model.access.csv',

		'data/market_segment_data.xml',

		'views/company_views.xml',
		'views/contact_views.xml',
		'views/site_views.xml',
		'views/configuration_views.xml',
		'views/customer_relationship_menus.xml',
	],
	'assets': {
		'web.assets_backend': [
			'customer_relationship/static/scss/copy_button.scss',
		]
	}
}
