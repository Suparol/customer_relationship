This is a custom [Odoo](https://www.odoo.com/) module, for customer relationship management, developped for specific needs.

Although code and variables are in English, most user-visible names are in French. This may change in the future.

The main module is 'customer_relationship'.
Other modules are "link modules" that auto-install when their requirements are met.
Finally, module 'customer_relationship_all' installs all link modules (and their dependancies).
